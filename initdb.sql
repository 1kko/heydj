CREATE TABLE IF NOT EXISTS playlist(
    id integer PRIMARY KEY AUTOINCREMENT,
    addDate timestamp NOT NULL,
    videoId text NOT NULL,
    title text,
    thumbs text,
    duration integer,
    rating float,
    addUser text
);

CREATE UNIQUE INDEX "idIndex" ON "playlist" ("id");

CREATE TABLE IF NOT EXISTS variables(
    id integer PRIMARY KEY AUTOINCREMENT,
    key text NOT NULL,
    value text
);

CREATE TABLE IF NOT EXISTS users(
    idx integer PRIMARY KEY AUTOINCREMENT,
    id text NOT NULL,
    lastActionTime timestamp,
    actionCount integer,
    displayName text
);


INSERT INTO "playlist" (
	"id", "addDate", "videoId", "title", "thumbs", "duration", "rating", "addUser"
	) VALUES (
	'1', '2019-11-22 02:38:11.097876', 'pWF1dNBkldU', 'J Rabbit - 웃으며 넘길래 (Smile)', 'http://i.ytimg.com/vi/pWF1dNBkldU/mqdefault.jpg', '00:03:56', '4.9538932', ''
);
INSERT INTO "variables" (key, value) VALUES ("currentIdx","1");