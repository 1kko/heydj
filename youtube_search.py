# Beautiful Soup is a Python library for pulling data out of HTML and XML files
# pip install beautifulsoup4

import requests
from bs4 import BeautifulSoup
import urllib.parse
import json


# def parse_html(self, soup):
#     results = []
#     for video in soup.select(".yt-uix-tile-link"):
#         if video["href"].startswith("/watch?v="):
#             print(video)
#             video_info = {
#                 "title": video["title"],
#                 "link": video["href"],
#                 "id": video["href"][video["href"].index("=")+1:],
#                 "duration": video.parent.parent.parent.select('.video-time')[0].text
#             }
#             results.append(video_info)
#     return results

class YoutubeSearch:

    def __init__(self, search_terms: str, max_results=None):
        self.BASE_URL = "https://youtube.com"
        self.search_terms = search_terms
        self.max_results = max_results
        self.videos = self.search()

    def search(self):
        encoded_search = urllib.parse.quote(self.search_terms)
        url = f"{self.BASE_URL}/results?search_query={encoded_search}&pbj=1"
        response = BeautifulSoup(requests.get(url).text, "html.parser")
        results = self.parse_html(response)
        if self.max_results is not None and len(results) > self.max_results:
            return results[:self.max_results]
        return results

    def parse_html(self, soup):
        results = []
        for video in soup.select(".yt-uix-tile-link"):
            if video["href"].startswith("/watch?v="):
                videoMeta=video.parent.parent.parent
                try:
                    videoId=video["href"][video["href"].index("=")+1:]
                    thumbs = f"https://i.ytimg.com/vi/{videoId}/mqdefault.jpg"
                    video_info = {
                        "title": video["title"],
                        "link": "youtu.be/"+videoId,
                        "id": videoId,
                        "duration": videoMeta.select('.video-time')[0].text,
                        'thumbs': thumbs,
                        'desc': videoMeta.select('.yt-lockup-description')[0].text
                    }
                    results.append(video_info)
                except:
                    pass

        return results

    def to_dict(self):
        return self.videos

    def to_json(self):
        return json.dumps({"videos": self.videos})