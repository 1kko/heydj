# HeyDJ

HeyDJ is a slack bot to play, queue music.

- - - -

## How to use

### 1. Add song:
  1. Join to slack channel
  1. Direct message to bot_name the youtube link. (eg. @HeyDJ https://www.youtube.com/1233456)
  1. Send

### 2. Send Commands:
  Send message to manipulate the song.
  1. play - to play song
  1. stop - to stop song
  1. pause - to pause song
  1. next - to skip the song.

- - - -

## Installation

### 0. Enable in virtual Env
using virtualenv is required when activating apache2 wsgi environment.

```
$ pip install virtualenv
$ virtualenv env
```
activate virtual environment
```
$ source env/bin/activate
(venv) $
```

### 1. Install dependencies.
```
pip install slackbot vlc pafy flask youtube-dl psutil opencv-contrib-python requests
```


### 2. Install VLC Player.
  [Download VLC Player](https://www.videolan.org/vlc/).

  Architecture of VLC Player and Python **should be same**.

  If your **Python** is **x64** then **VLC Player** must be **x64** too.


### 3. Create Slack Bot Token

  1. Create a [Slack app](https://api.slack.com/apps?new_app=1)

  1. Add a Bot User and configure your bot user with some basic info (display name, default username and its online presence)

  1. Once you've completed these fields, click **Add Bot User**

  1. Next, give your bot access to the [Events API](https://api.slack.com/bot-users#setup-events-api)

  1. Finally, add your [bot to your workspace](https://api.slack.com/bot-users#installing-bot)

> More details https://slack.com/intl/en-vn/help/articles/115005265703


### 4. Put your slackbot token to configuration.

Rename the `slackbot_settings.py_default` to `slackbot_settings.py` fill up the API token starting with `xoxb`
```
API_TOKEN = "xoxb-something-very-long-string"
```


### 5. Create database
  1. Change code to create `playlist.db`

    Open webserver.py, and goto bottom line where `myPlayer.runOnce(False)`
Change `False` to `True`

  1. On the command line:

    ```$ python webserver.py```

  1. Once the `playlist.db` is created, change the code back to `False`

### 6. Finally, Run!!

```
$ python start.py
```

### 7. (optional) use wsgi in apache2

modify `heydj_apache-site.conf` to your directory and ip address
modify `webserver.wsgi` to your directory
```
sudo apt-get install libapache2-mod-wsgi-py3 python-dev
sudo cp heydj_apache-site.conf /etc/apache2/sites-enabled
sudo a2ensite heydj_apache-site.conf
sudo service apache2 reload
```



- - - -
## About Internal Structure
HeyDJ relies on vlc library and flask for playing music and communicating with bot service. 

webserver.py receives `GET` request and runs the command.
botserver.py receives slack message from user using API, and sends http request to webserver and receives the return, gives user feed back in message format.


### shebang
The #! syntax used in scripts to indicate an interpreter for execution under UNIX / Linux operating systems.
More detail at: https://bash.cyberciti.biz/guide/Shebang.

### webserver.py
This is a mixture of vlc, sqlite3 and flask.
When request is received from the http, it will respond in json.

#### vlc
I'm using vlc on MediaPlayer.
I use the MediaPlayer and create new instance everytime new music needs to played. When music stops, or skipped the MediaPlayer instance terminates.

#### User request limit
Basically Slack doesn't allow to send message more than 1 per second. Although, to avoid the abuse of adding the music or skipping other's music, I hard-coded `maxReqCount`, and `maxReqTime`. 

### playlist.db
This database stores userdata and playlist requested from user.
Index of current playing media is also saved to `variables.currentIdx`.

### botserver.py
This opens up slack bot session using Real Time Messaging API, due to the firewall restriction.
Also receives the message and parse them and send request to `webserver.py`.

### start.py
You can start webserver.py and botserver.py separately

In command line window:
```
$ python webserver.py
```
Open another command line window:
```
$ python botserver.py
```

However, you can run them both at once like:
```
$ python start.py
```

